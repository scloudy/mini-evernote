Rails.application.routes.draw do
  get 'notes', to: 'notes#index', as: 'notes'

  get 'notes/new'

  post 'notes', to: 'notes#create'

  get 'notes/:id', to: 'notes#show', as: 'note'

  get 'notes/:id/edit', to: 'notes#edit', as: 'edit_note'

  patch 'notes/:id', to: 'notes#update'

  delete 'notes/:id', to: 'notes#destroy'

  root 'notes#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
